﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity Get(int id);
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> GetRange(Expression<Func<TEntity, int>> orderExpression, int startIndex, int count);
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, int>> orderExpression, int startIndex, int count);

        TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate);

        void Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);

        int Count();
        int Count(Expression<Func<TEntity, bool>> predicate);

        void Remove(int id);
        void Remove(TEntity entity);
        void RemoveRange(IEnumerable<TEntity> entities);

        void Update(int idOfUpadtedEntity, TEntity updatedEntity);
        void UpdateSingle(Expression<Func<TEntity, bool>> matchPredicate, TEntity updatedEntity);
    }
}
