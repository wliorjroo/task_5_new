﻿using DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository.Factory
{
    public interface IRepositoryFactory
    {
        IRepository<TModel> CreateRepository<TModel>() where TModel : class;
    }
}
