namespace WebUI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'67108728-bf5a-403e-a833-47c2cfa7ba1b', N'admin@mvcApp.com', 0, N'ANBntHMNpqvLbABApSOiwCojFHyK05cKuDxYRPxdHmuPX3/EMRKKYUskDVLE+GbvqA==', N'59cd0ce7-bec2-4385-abe6-ee675f81f8e2', NULL, 0, 0, NULL, 1, 0, N'admin@mvcApp.com')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'641b5fc7-4c0a-4414-9fa6-adc6c20f5455', N'user@mvcApp.com', 0, N'ANBntHMNpqvLbABApSOiwCojFHyK05cKuDxYRPxdHmuPX3/EMRKKYUskDVLE+GbvqA==', N'61105df8-79c3-4775-9db0-260bfa8db86d', NULL, 0, 0, NULL, 1, 0, N'user@mvcApp.com')
INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'd3c97f27-3868-4ee7-93ba-c4ad5eafe3d9', N'Write')
INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'c8dedd78-66eb-4ae4-ae1a-21cda6c7ca9c', N'Read')
INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'67108728-bf5a-403e-a833-47c2cfa7ba1b', N'd3c97f27-3868-4ee7-93ba-c4ad5eafe3d9')
INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'67108728-bf5a-403e-a833-47c2cfa7ba1b', N'c8dedd78-66eb-4ae4-ae1a-21cda6c7ca9c')
INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'641b5fc7-4c0a-4414-9fa6-adc6c20f5455', N'c8dedd78-66eb-4ae4-ae1a-21cda6c7ca9c')
");
        }
        
        public override void Down()
        {
        }
    }
}
