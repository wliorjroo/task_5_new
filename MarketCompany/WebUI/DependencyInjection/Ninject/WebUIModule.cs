﻿using Ninject.Modules;

namespace WebUI.DependencyInjection.Ninject
{
    public class WebUIModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Load(new[] { new DAL.DI.Ninject.DALModule() });
        }
    }
}
