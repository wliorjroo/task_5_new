﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace DAL.UnitOfWork
{
    public interface IGenericUnitOfWork<TModel> where TModel : class
    {
        TModel GetById(int id);
        IEnumerable<TModel> GetAll();
        IEnumerable<TModel> Find(Expression<Func<TModel, bool>> predicate);
        void Remove(int id);
        void AddOrUpdate(int modelId, TModel model);
    }
}
