﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Mapping.Initialize
{
    class OrderProfile : Profile
    {
        public OrderProfile()
        {
            CreateDataToModel();
            CreateModelToData();
        }

        private void CreateDataToModel()
        {
            var mapping = CreateMap<DataSource.Order, Model.Order>();
            mapping.ForMember(d => d.CustomerId, m => m.MapFrom(s => s.ClientId));
            mapping.ForMember(d => d.ProductId, m => m.MapFrom(s => s.ProductId));
            mapping.ForMember(d => d.SellerId, m => m.MapFrom(s => s.UserId));
            mapping.ForMember(d => d.Customer, m => m.MapFrom(s => s.Client));
            mapping.ForMember(d => d.Seller, m => m.MapFrom(s => s.User));
        }

        private void CreateModelToData()
        {
            var mapping = CreateMap<Model.Order, DataSource.Order>();
            mapping.ForMember(d => d.ClientId, m => m.MapFrom(s => s.CustomerId));
            mapping.ForMember(d => d.ProductId, m => m.MapFrom(s => s.ProductId));
            mapping.ForMember(d => d.UserId, m => m.MapFrom(s => s.SellerId));
            mapping.ForMember(d => d.Client, m => m.MapFrom(s => s.Customer));
            mapping.ForMember(d => d.User, m => m.MapFrom(s => s.Seller));
        }
    }
}
