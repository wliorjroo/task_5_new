﻿using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.RepositoryContainer
{
    public interface IRepositoryContainer
    {
        IRepository<TModel> GetRepository<TModel>() where TModel : class;
    }
}
