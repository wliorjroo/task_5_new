﻿using AutoMapper;

namespace WebUI.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<DAL.Model.Customer, Models.Customer>();

            CreateMap<Models.Customer, DAL.Model.Customer>();
        }
    }
}