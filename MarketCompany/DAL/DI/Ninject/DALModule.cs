﻿using AutoMapper;
using DAL.Mapping.Initialize;
using DAL.Repository;
using DAL.UnitOfWork;
using Ninject.Modules;
using System.Threading;

namespace DAL.DI.Ninject
{
    public class DALModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Load(new[] { new DataSource.DI.Ninject.DataSourceModule() });

            Bind<IMapper>().ToConstant(AutoMapperFactory.Create());

            Bind<IRepository<DataSource.Client>>().To<Repository<DataSource.Client>>().InThreadScope();
            Bind<IRepository<DataSource.Order>>().To<Repository<DataSource.Order>>().InThreadScope();
            Bind<IRepository<DataSource.Product>>().To<Repository<DataSource.Product>>().InThreadScope();
            Bind<IRepository<DataSource.User>>().To<Repository<DataSource.User>>().InThreadScope();

            Bind<IRepository<Model.Customer>>().To<AutoMappedRepository<Model.Customer, DataSource.Client>>().InThreadScope();
            Bind<IRepository<Model.Order>>().To<AutoMappedRepository<Model.Order, DataSource.Order>>().InThreadScope();
            Bind<IRepository<Model.Product>>().To<AutoMappedRepository<Model.Product, DataSource.Product>>().InThreadScope();
            Bind<IRepository<Model.Seller>>().To<AutoMappedRepository<Model.Seller, DataSource.User>>().InThreadScope();

            Bind<ReaderWriterLockSlim>().To<ReaderWriterLockSlim>().InSingletonScope();

            Bind<IGenericUnitOfWork<Model.Customer>>().To<GenericUnitOfWork<Model.Customer>>().InThreadScope();
        }
    }
}
