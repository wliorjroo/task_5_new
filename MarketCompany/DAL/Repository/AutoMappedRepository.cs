﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    class AutoMappedRepository<TModel, TEntity> : IRepository<TModel> where TModel : class where TEntity : class
    {
        private readonly IRepository<TEntity> _repository;
        private readonly IMapper _mapper;

        public AutoMappedRepository(IRepository<TEntity> repository, IMapper mapper)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public void Add(TModel item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            _repository.Add(_mapper.Map<TModel, TEntity>(item));
        }

        public void AddRange(IEnumerable<TModel> models)
        {
            _repository.AddRange(_mapper.Map<IEnumerable<TModel>, IEnumerable<TEntity>>(models));
        }

        public int Count()
        {
            return _repository.Count();
        }

        public int Count(Expression<Func<TModel, bool>> predicate)
        {
            return _repository.Count(_mapper.Map<Expression<Func<TEntity, bool>>>(predicate));
        }

        public void Remove(int id)
        {
            _repository.Remove(id);
        }

        public void Remove(TModel model)
        {
            _repository.Remove(_mapper.Map<TEntity>(model));
        }

        public void RemoveRange(IEnumerable<TModel> models)
        {
            _repository.RemoveRange(_mapper.Map<IEnumerable<TEntity>>(models));
        }

        public void UpdateSingle(Expression<Func<TModel, bool>> matchPredicate, TModel model)
        {
            var expression = _mapper.Map<Expression<Func<TEntity, bool>>>(matchPredicate);
            _repository.UpdateSingle(expression, _mapper.Map<TEntity>(model));
        }

        public void Update(int idOfUpadtedEntity, TModel updatedModel)
        {
            _repository.Update(idOfUpadtedEntity, _mapper.Map<TEntity>(updatedModel));
        }

        public TModel Get(int id)
        {
            var entity = _repository.Get(id);
            if (entity != null)
                return _mapper.Map<TEntity, TModel>(entity);
            return null;
        }

        public IEnumerable<TModel> GetAll()
        {
            var all = _repository.GetAll().ToList();
            return _mapper.Map<IEnumerable<TEntity>, IEnumerable<TModel>>(all);
            //return _mapper.Map<IEnumerable<TModel>>(_repository.GetAll());
        }

        public IEnumerable<TModel> GetRange(Expression<Func<TModel, int>> orderExpression, int startIndex, int count)
        {
            var result = _repository.GetRange(_mapper.Map<Expression<Func<TEntity, int>>>(orderExpression) , startIndex, count);
            return _mapper.Map<IEnumerable<TModel>>(result);
        }

        public IEnumerable<TModel> Find(Expression<Func<TModel, bool>> predicate)
        {
            var expression = _mapper.Map<Expression<Func<TEntity, bool>>>(predicate);
            return _mapper.Map<IEnumerable<TModel>>(_repository.Find(expression));
        }

        public IEnumerable<TModel> Find(Expression<Func<TModel, bool>> predicate, Expression<Func<TModel, int>> orderExpression, int startIndex, int count)
        {
            var result = _repository.Find(_mapper.Map<Expression<Func<TEntity, bool>>>(predicate), _mapper.Map<Expression<Func<TEntity, int>>>(orderExpression), startIndex, count);
            return _mapper.Map<IEnumerable<TModel>>(result);
        }

        public TModel SingleOrDefault(Expression<Func<TModel, bool>> predicate)
        {
            var expression = _mapper.Map<Expression<Func<TEntity, bool>>>(predicate);
            return _mapper.Map<TEntity, TModel>(_repository.SingleOrDefault(expression));
        }
    }
}
