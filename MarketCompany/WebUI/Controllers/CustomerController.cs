﻿using AutoMapper;
using DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace WebUI.Controllers
{
    [Authorize(Roles = Models.RoleName.Read)]
    public class CustomerController : Controller
    {
        private readonly IGenericUnitOfWork<DAL.Model.Customer> _genericUnitOfWork;

        public CustomerController(IGenericUnitOfWork<DAL.Model.Customer> genericUnitOfWork)
        {
            _genericUnitOfWork = genericUnitOfWork ?? throw new ArgumentNullException(nameof(genericUnitOfWork));
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Customers";
            var model = Mapper.Map<IEnumerable<Models.Customer>>(_genericUnitOfWork.GetAll());

            if (User.IsInRole(Models.RoleName.Write))
                return View("IndexWrite", model);
            return View(model);
        }

        [Authorize(Roles = Models.RoleName.Write)]
        public ActionResult New()
        {
            var viewModel = Mapper.Map<Models.Customer>(new DAL.Model.Customer());
            ViewBag.Title = "New Customer";
            return View("CustomerForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = Models.RoleName.Write)]
        public ActionResult Save(Models.Customer customer)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Title = "Fix errors";
                return View("CustomerForm", customer);
            }

            _genericUnitOfWork.AddOrUpdate(customer.Id, Mapper.Map<DAL.Model.Customer>(customer));

            return RedirectToAction("Index", "Customer");
        }

        public ActionResult Details(int id)
        {
            var customer = _genericUnitOfWork.GetById(id);

            if (customer == null)
                return HttpNotFound();

            var viewModel = Mapper.Map<Models.Customer>(customer);
            ViewBag.Title = "Details";
            return View(viewModel);
        }

        [Authorize(Roles = Models.RoleName.Write)]
        public ActionResult Edit(int id)
        {
            var customer = _genericUnitOfWork.GetById(id);

            if (customer == null)
                return HttpNotFound();

            var viewModel = Mapper.Map<Models.Customer>(customer);
            ViewBag.Title = "Edit";
            return View("CustomerForm", viewModel);
        }

        [HttpGet]
        [Authorize(Roles = Models.RoleName.Write)]
        public void Delete(int id)
        {
            var customer = _genericUnitOfWork.GetById(id);

            if (customer == null)
                return;// HttpNotFound();

            _genericUnitOfWork.Remove(id);
        }
    }
}