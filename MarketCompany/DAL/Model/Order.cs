﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model
{
    public class Order
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int ProductCount { get; set; }
        public int CustomerId { get; set; }
        public int ProductId { get; set; }
        public int SellerId { get; set; }

        public Customer Customer { get; set; }
        public Product Product { get; set; }
        public Seller Seller { get; set; }
    }
}
