﻿using Ninject;
using Ninject.Web.Mvc;
using System.Web.Mvc;

namespace WebUI.DependencyInjection.Ninject
{
    class DependencyResolver : NinjectDependencyResolver
    {
        public DependencyResolver(IKernel kernel) : base(kernel)
        {
            kernel.Unbind<ModelValidatorProvider>();
            kernel.Load(new DAL.DI.Ninject.DALModule());
        }
    }
}