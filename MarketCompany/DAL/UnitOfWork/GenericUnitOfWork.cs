﻿using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DAL.UnitOfWork
{
    class GenericUnitOfWork<TModel> : IGenericUnitOfWork<TModel> where TModel : class
    {
        private readonly IRepository<TModel> _repository;
        private readonly DbContext _dbContext;
        private readonly ReaderWriterLockSlim _readerWriterLock;

        public GenericUnitOfWork(IRepository<TModel> repository, DbContext dbContext, ReaderWriterLockSlim readerWriterLock)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _readerWriterLock = readerWriterLock ?? throw new ArgumentNullException(nameof(readerWriterLock));
        }

        private void SaveChanges()
        {
            _readerWriterLock.EnterWriteLock();
            try
            {
                _dbContext.SaveChanges();
            }
            finally
            {
                _readerWriterLock.ExitWriteLock();
            }
        }

        public void AddOrUpdate(int modelId, TModel model)
        {
            if (_repository.Get(modelId) == null)
            {
                _repository.Add(model);
            }
            else
            { 
                _repository.Update(modelId, model); 
            }
            SaveChanges();
        }

        public IEnumerable<TModel> Find(Expression<Func<TModel, bool>> predicate)
        {
            return _repository.Find(predicate);
        }

        public IEnumerable<TModel> GetAll()
        {
            return _repository.GetAll();
        }

        public TModel GetById(int id)
        {
            return _repository.Get(id);
        }

        public void Remove(int id)
        {
            if (_repository.Get(id) == null)
            {
                _repository.Remove(id);
                SaveChanges();
            }
            else
            {
                throw new InvalidOperationException();
            }
        }
    }
}
